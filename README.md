# Ansirecon

## Ansible playbooks to automate repetitive pentesting tasks

Meant for CTF's and pentests where discovery of your attacks is not a concern. 

Not built to be used on red team or other covert engagements, so don't call me from gitmo if you mess up
